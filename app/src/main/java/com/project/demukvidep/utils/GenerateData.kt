package com.project.demukvidep.utils

import android.content.Context
import com.google.gson.Gson
import com.project.demukvidep.R
import com.project.demukvidep.data.Content
import com.project.demukvidep.data.Playlist
import java.io.IOException

class GenerateData(var context: Context?) {

    var list = listOf(R.raw.demuk20200602_165149_731, R.raw.demuk20200605_103308_392, R.raw.demuk20200605_103313_540)

    fun getDataJson(): Array<Playlist>? {
        var json: String? = null
        try {
            val inputStream = context!!.assets.open("json/playlist.json")
            val bytes = ByteArray(inputStream.available())
            inputStream.read(bytes, 0, bytes.size)
            json = String(bytes)
        } catch (e: IOException) {
        }
        return Gson().fromJson(json, Array<Playlist>::class.java)
    }

    fun getListofVideo(): List<Int> = list

    fun getPlayList(): Array<Playlist>? = getDataJson()

}