package com.project.demukvidep

import android.app.Application
import com.project.demukvidep.di.appModule
import com.project.demukvidep.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        initkoin()
    }

    private fun initkoin() {
        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(appModule+ networkModule)
        }
    }
}