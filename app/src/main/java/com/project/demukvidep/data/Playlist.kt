package com.project.demukvidep.data

data class Playlist(
    val id: String,
    val name: String,
    val contents: List<Content>
)