package com.project.demukvidep.data.api

import retrofit2.http.GET
import retrofit2.http.POST

interface IPlaylist {

    @GET("")
    suspend fun getPlaylist()
}