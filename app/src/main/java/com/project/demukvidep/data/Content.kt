package com.project.demukvidep.data

import com.google.gson.annotations.SerializedName

data class Content(
    @SerializedName("@type")
    val type: String,
    val activeDate: String,
    val backgroundColor: Any,
    val command: Any,
    val computer: Any,
    val direction: Any,
    val displayMode: String,
    val duration: Double,
    val durationInSecond: String,
    val expireDate: String,
    val height: Int,
    val id: String,
    val ledcolor: String,
    val name: String,
    val opacity: Double,
    val password: Any,
    val playlistId: Any,
    val server: Any,
    val speed: Any,
    val suffix: Any,
    val templateId: Any,
    val textColor: Any,
    val textMessages: Any,
    val textSize: Int,
    val timemode: String,
    val timestart: String,
    val timestop: String,
    val topic: Int,
    val uri: String,
    val username: Any,
    val volume: String,
    val width: Int
)