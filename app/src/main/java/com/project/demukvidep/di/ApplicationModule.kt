package com.project.demukvidep.di

import com.project.demukvidep.ui.PlayListViewModel
import com.project.demukvidep.utils.GenerateData
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
//    single {
//        ArtistRepository(get())
//    }
//    factory {
//        GetArtistsUseCase(get())
//    }
    single { GenerateData(androidContext())}
    viewModel { PlayListViewModel(get()) }
}