package com.project.demukvidep.di

import com.project.demukvidep.data.api.IPlaylist
import com.project.demukvidep.data.builder.OkHttpBuilder
import com.project.demukvidep.data.builder.RetrofitBuilder
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
private const val BASE_URL = ""

val networkModule = module {

    single { OkHttpBuilder().build() }

    single<CallAdapter.Factory> { RxJava2CallAdapterFactory.create() }

    single<Converter.Factory> { GsonConverterFactory.create() }

    single { RetrofitBuilder(get(), get(), get()) }

    single<IPlaylist> { get<RetrofitBuilder>().build(BASE_URL) }
}