package com.project.demukvidep

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.project.demukvidep.data.Playlist
import com.project.demukvidep.ui.PlayListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: PlayListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestPermission()
    }

    private fun initvideo(resId: List<Int>,i: Int) {
        var count = i
        val id = resId[count]
        val videoPath = "android.resource://$packageName/$id"
        val uri: Uri = Uri.parse(videoPath)
        videoView.setVideoURI(uri)
        videoView.requestFocus()
        videoView.start()
        val mediaController = MediaController(this)
        videoView.setMediaController(mediaController)
        mediaController.setAnchorView(videoView)
        videoView.setOnCompletionListener {
            if(count == 2){
                count = 0
                initvideo(resId,count)
            }else{
                count++
                initvideo(resId,count)
            }
        }
    }


    private fun readDataFromjson() {
        viewModel.getJsonPlaylist()
        viewModel.playListLiveData.observe(this, Observer {
          it.forEachIndexed {i, f->
              createPlayer(f)
          }
        })
    }

    private fun createPlayer(list: Playlist) {
        list.contents.forEach {
            val char = "demuk"
            val uri = "$char${it.uri}"
            playVideo(uri)
        }

    }

    private fun playVideo(uri: String) {
        viewModel.getlocalVideo()
        viewModel.listofvideoLiveData.observe(this, Observer {
            initvideo(it,0)
        })
    }

    private fun requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            //Ask for permission
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 0)
        } else {
            readDataFromjson()
        }
    }

}
