package com.project.demukvidep.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.coroutines.CoroutineContext
import androidx.lifecycle.viewModelScope
import com.project.demukvidep.data.Playlist
import com.project.demukvidep.utils.GenerateData
import kotlinx.coroutines.*

class PlayListViewModel(private val generateData: GenerateData) : ViewModel(), CoroutineScope {

    val playListLiveData: MutableLiveData<Array<Playlist>> = MutableLiveData()

    val listofvideoLiveData: MutableLiveData<List<Int>> = MutableLiveData()

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main


    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    fun getJsonPlaylist() {
        viewModelScope.launch(Dispatchers.IO) {
            val reponse: Array<Playlist>? = withContext(Dispatchers.IO) {
                generateData.getDataJson()
            }
            playListLiveData.postValue(reponse)
        }
    }

    fun getlocalVideo() {
        viewModelScope.launch(Dispatchers.IO) {
            val reponse: List<Int>? = withContext(Dispatchers.IO) {
                generateData.getListofVideo()
            }
            listofvideoLiveData.postValue(reponse)
        }
    }
}
